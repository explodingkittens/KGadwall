package xyz.benhigh.kgadwall

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import kotlin.math.roundToInt
import xyz.benhigh.kgadwall.backend.Item

/**
 * Component to show an item from the shopping cart.
 *
 * This component holds a TextView and a CheckBox component to show a shopping
 * cart item. The LinearLayout encompassing these two elements also has a click
 * handler which changes the checked/unchecked value of the CheckBox.
 *
 * @author Ben High
 */
class ShoppingListItemComponent(context: Context, item: Item) {
	// Create components
	val layoutItem = LinearLayout(context)
	private val textView = TextView(context)
	val checkBox = CheckBox(context)

	init {
		// Set up a function to convert px to dp
		val density = context.resources.displayMetrics.density
		fun toDp(px: Int): Int = (px.toFloat() * density).roundToInt()

		// Configure layoutItem
		val linearLayoutParams = LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT
		)
		linearLayoutParams.setMargins(0, 0, 0, toDp(1))
		layoutItem.layoutParams = linearLayoutParams
		layoutItem.setPadding(0, toDp(10), 0, toDp(10))
		layoutItem.setBackgroundColor(ContextCompat.getColor(context, R.color.colorItemBackground))
		layoutItem.gravity = Gravity.CENTER
		layoutItem.setOnClickListener {
			checkBox.isChecked = !checkBox.isChecked
		}

		// Configure text view
		val textViewParams = LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT
		)
		textViewParams.weight = 10.0f
		textView.layoutParams = textViewParams
		textView.setPadding(toDp(3), 0, 0, 0)
		textView.textSize = 18f
		textView.setTextColor(Color.BLACK)
		textView.typeface = Typeface.create("sans-serif-condensed", Typeface.NORMAL)
		textView.text = item.name

		// Add a tag to the checkbox for the edit meal activity
		checkBox.tag = "checkbox"

		// Add children to layout
		layoutItem.addView(textView)
		layoutItem.addView(checkBox)
	}

}