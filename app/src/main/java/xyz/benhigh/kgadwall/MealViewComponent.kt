package xyz.benhigh.kgadwall

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.widget.LinearLayout
import android.widget.TextView
import xyz.benhigh.kgadwall.backend.Meal
import kotlin.math.roundToInt

/**
 * Component to display a meal's name and all associated items.
 *
 * This component, used on MealsActivity, draws a LinearLayout object with a
 * title TextView displaying the name, and then smaller TextView components for
 * each item linked to the passed in meal object. When used in an activity, the
 * layoutItem component is what should be added to the screen. When used in an
 * activity, the layoutItem component is what should be added to the screen.
 *
 * @author Ben High
 */
class MealViewComponent(context: Context, meal: Meal) {
	val layoutItem = LinearLayout(context)
	private val titleTextView = TextView(context)

	init {
		// Set up a function to convert px to dp
		val density = context.resources.displayMetrics.density
		fun toDp(px: Int): Int = (px.toFloat() * density).roundToInt()

		// Set up the linear layout's settings
		val linearLayoutParams = LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT
		)
		linearLayoutParams.setMargins(0, 0, 0, toDp(1))
		layoutItem.layoutParams = linearLayoutParams
		layoutItem.setPadding(0, toDp(5), 0, toDp(5))
		layoutItem.setBackgroundColor(ContextCompat.getColor(context,
												R.color.colorMealBackground))
		layoutItem.orientation = LinearLayout.VERTICAL

		// Set up the title text's settings
		titleTextView.text = meal.name
		titleTextView.textSize = 22f
		titleTextView.setPadding(toDp(3), 0, 0, 0)
		titleTextView.setTextColor(Color.BLACK)
		titleTextView.typeface = Typeface.create("sans-serif-condensed",
												 Typeface.NORMAL)

		// Add the title now
		layoutItem.addView(titleTextView)

		// Add text views for all items
		for (item in meal.items) {
			val itemText = TextView(context)
			itemText.text = item.name
			itemText.textSize = 14f
			itemText.setPadding(toDp(20), 0, 0, 0)
			itemText.setTextColor(ContextCompat.getColor(context,
													R.color.colorMealItemText))
			itemText.typeface = Typeface.create("sans-serif-condensed",
												Typeface.NORMAL)

			layoutItem.addView(itemText)
		}
	}
}