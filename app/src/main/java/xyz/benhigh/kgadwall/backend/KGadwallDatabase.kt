package xyz.benhigh.kgadwall.backend

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

/**
 * Abstract class to hold and return DAO objects.
 *
 * @author Ben High
 */
@Database(entities = [(Item::class), (Meal::class), (ItemMealRelation::class)],
		  version = 1)
abstract class KGadwallDatabase: RoomDatabase() {
	abstract fun itemDao(): ItemDao
	abstract fun mealDao(): MealDao
	abstract fun itemMealRelationDao(): ItemMealRelationDao
}