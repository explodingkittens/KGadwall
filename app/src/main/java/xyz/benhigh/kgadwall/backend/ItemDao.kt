package xyz.benhigh.kgadwall.backend

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query

/**
 * Data Access Object interface for item objects.
 *
 * This interface defines SQL queries for the items table in the database. This
 * interface provides methods for selecting, inserting, and deleting items from
 * the table.
 *
 * @author Ben High
 */
@Dao
interface ItemDao {
	/**
	 * Method to return a list of all items in the table.
	 *
	 * @return A list of all items in the table.
	 */
	@Query("SELECT * FROM Items")
	fun getAll(): List<Item>

	/**
	 * Method to insert an item into the table.
	 *
	 * If an item with an existing GUID is passed in, the entry in the database
	 * is updated instead.
	 *
	 * @param item The item object to be inserted or updated.
	 */
	@Insert(onConflict = REPLACE)
	fun insert(item: Item)

	/**
	 * Method to clear the items table of all entries.
	 */
	@Query("DELETE FROM Items")
	fun emptyTable()

	/**
	 * Method to delete an item from the table by ID.
	 *
	 * @param id The GUID value of the item to be deleted.
	 */
	@Query("DELETE FROM Items WHERE id=:id")
	fun deleteItemByID(id: Int)
}