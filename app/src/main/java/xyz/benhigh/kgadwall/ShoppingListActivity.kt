package xyz.benhigh.kgadwall

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.view.animation.AlphaAnimation
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_shopping_list.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import xyz.benhigh.kgadwall.backend.Item
import xyz.benhigh.kgadwall.backend.ShoppingCart

/**
 * Activity to show all items in the cart and allow the user to tap those items
 * to dismiss them from the cart.
 *
 * This is the main launcher activity for KGadwall. This screen either shows
 * all of the items in the database that are marked as being in the cart, or it
 * shows a screen telling the user that their cart is empty, depending on if
 * there are any items in the cart.
 *
 * @author Ben High
 */
class ShoppingListActivity : AppCompatActivity() {
	private val cart = ShoppingCart()

	/**
	 * Initialize the activity.
	 *
	 * Nothing much has changed here, simply initialize the activity as usual.
	 *
	 * @param savedInstanceState The save state for the activity.
	 */
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_shopping_list)
	}

	/**
	 * Show a loading screen and asynchronously reload the data when resuming.
	 *
	 * This activity could be resumed from the make list or meals activity, so
	 * we want to reload the data here to make sure that the frontend does not
	 * fall behind the backend.
	 */
	override fun onResume() {
		super.onResume()

		// Show loading screen while waiting for data to load
		shoppingListLayout.removeAllViews()
		shoppingListLayout.addView(MessageScreenComponent(this, R.string.cart_loading_message, R.drawable.ic_storage_accent_24dp, false).layoutItem)

		// Load data from the database
		loadData(this)
	}

	/**
	 * Initializes the options menu.
	 *
	 * This method initializes the options menu for the action bar, found in
	 * /res/menu/shopping_list_menu.xml.
	 *
	 * @param menu The menu in which to inflate our menu.
	 * @return Always true because we want to show the options menu.
	 */
	override fun onCreateOptionsMenu(menu: Menu?): Boolean {
		menuInflater.inflate(R.menu.shopping_list_menu, menu)
		return true
	}

	/**
	 * Bind actions to the make list and meals menu buttons.
	 *
	 * The make list button launches MakeListActivity. The meals button
	 * launches MealsActivity.
	 *
	 * @param item The selected menu item.
	 * @return Always true because we want to consume the tap action.
	 */
	override fun onOptionsItemSelected(item: MenuItem?): Boolean {
		when {
			item!!.itemId == R.id.makeListMenuButton -> {
				val intent = Intent(this, MakeListActivity::class.java)
				startActivity(intent)
			}
			item.itemId == R.id.mealsMenuButton -> {
				val intent = Intent(this, MealsActivity::class.java)
				startActivity(intent)
			}
		}
		return true
	}

	/**
	 * Add items to the screen.
	 *
	 * This method cycles through all items in the shopping list, adding
	 * entries for any items that are in the cart, and ignoring those that are
	 * not. Additionally, this method keeps track of the previous item it added
	 * to the screen, in order to determine if a new aisle marker is required.
	 * If no items are found in the cart, a message screen is displayed to
	 * inform the user that the list is empty. This method also sets up the
	 * onCheckedChangeListener for each component to change the item's inCart
	 * value when tapped.
	 */
	private fun populateScreen() {
		// Clear screen
		shoppingListLayout.removeAllViews()

		// Add cart items to the screen
		var lastItem: Item? = null
		for (item in cart.items) {
			if (item.inCart) {
				// Add an aisle marker if this item's aisle differs from the
				// previous item's aisle
				if (lastItem?.aisle != item.aisle) {
					val aisleMarker = TextView(this)
					aisleMarker.text = item.aisle
					aisleMarker.textSize = 24f
					aisleMarker.typeface = Typeface.create("sans-serif",
														   Typeface.BOLD)
				}

				val component = ShoppingListItemComponent(this, item)

				// Add the item to the cart when the checkbox is checked
				component.checkBox.setOnCheckedChangeListener { _, value ->
					if (value) {
						// Update backend
						item.inCart = false
						item.lastEdit = System.currentTimeMillis() / 1000
						saveData()

						// Fade out the item
						val fadeAnimation = AlphaAnimation(1.0f, 0.0f)
						fadeAnimation.duration = 500
						component.layoutItem.startAnimation(fadeAnimation)

						// Redraw the screen after the component has had some
						// time to fade out completely
						Handler().postDelayed({
							populateScreen()
						}, 500)
					}
				}

				shoppingListLayout.addView(component.layoutItem)
				lastItem = item
			}
		}

		// Add the cart empty screen if no items were added to the screen
		if (shoppingListLayout.childCount == 0) {
			shoppingListLayout.addView(MessageScreenComponent(
								this,
								R.string.cart_empty_message,
								R.drawable.ic_check_accent_24dp).layoutItem)
		}
	}

	/**
	 * (Asynchronous) Load items into the cart and populate the screen.
	 *
	 * This method initializes the database, and loads in all of the items from
	 * the database, into the cart. Then, the method invokes a UI thread to
	 * populate the screen with data.
	 *
	 * @param context The application context, for database initialization.
	 */
	private fun loadData(context: Context) = doAsync {
		cart.initializeDB(context)
		cart.loadItems()

		uiThread {
			populateScreen()
		}
	}

	/**
	 * (Asynchronous) Save the items in the cart.
	 *
	 * This method makes a call to the database (which should have already been
	 * initialized by loadData by now) to save the items in the cart to the
	 * database.
	 */
	private fun saveData() = doAsync {
		cart.saveItems()
	}
}
