package xyz.benhigh.kgadwall

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_make_list.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import xyz.benhigh.kgadwall.backend.ShoppingCart

/**
 * Activity to add items to the shopping list.
 *
 * This activity displays all items in the cart, regardless of whether or not
 * they are on the list. It takes nothing in as a bundle, but does load items
 * from the database. Additionally, this activity allows the user to access the
 * EditItemActivity to add, edit, or remove items.
 *
 * @author Ben High
 */
class MakeListActivity : AppCompatActivity() {
	private val cart = ShoppingCart()

	/**
	 * Initialize the activity.
	 *
	 * Nothing much has changed here, simply initialize the activity as usual.
	 *
	 * @param savedInstanceState The save state for the activity.
	 */
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_make_list)
	}

	/**
	 * Reload the data when the activity is resumed.
	 *
	 * We want to reload from the database whenever this activity is resumed,
	 * because this activity can be resumed from the EditItemActivity. If we
	 * did not reload here, then the list frontend would fall behind the
	 * backend.
	 */
	override fun onResume() {
		super.onResume()

		// Load items from the database
		loadData(this)
	}

	/**
	 * Initializes the options menu.
	 *
	 * This method initializes the options menu for the action bar, found in
	 * /res/menu/make_list_menu.xml.
	 *
	 * @param menu The menu in which to inflate our menu.
	 * @return Always true because we want to show the options menu.
	 */
	override fun onCreateOptionsMenu(menu: Menu?): Boolean {
		menuInflater.inflate(R.menu.make_list_menu, menu)
		return true
	}

	/**
	 * Binds actions to the add item and back menu buttons.
	 *
	 * The add item button launches the edit item activity with a passed in
	 * item ID of 0 (create a new item). The back button finishes the activity.
	 *
	 * @param item The selected menu item.
	 * @return Always true because we want to consume the tap action.
	 */
	override fun onOptionsItemSelected(item: MenuItem?): Boolean {
		when {
			item!!.itemId == R.id.addNewItemMenuButton -> {
				val intent = Intent(this,
						            EditItemActivity::class.java).apply {
					putExtra("itemID", 0)
				}
				startActivity(intent)
			}
			item.itemId == android.R.id.home -> {
				finish()
			}
		}
		return true
	}

	/**
	 * Add items to the screen.
	 *
	 * This method either adds all items to the screen, regardless of whether
	 * or not they're in the cart, or adds a message saying there are no items,
	 * depending on whether or not the cart contains any items.
	 */
	private fun populateScreen() {
		editListView.removeAllViews()

		// Only try to add items to the screen when there are items to add.
		// Otherwise, show the empty list screen.
		if (cart.items.isNotEmpty()) {
			for (item in cart.items) {
				// Create the item component and set the checkbox
				val component = ShoppingListItemComponent(this, item)
				component.checkBox.isChecked = (item.inCart)

				// Add or remove the item to the cart when clicked, and then
				// launch an asynchronous thread to save items in the database
				component.checkBox.setOnCheckedChangeListener { _, value ->
					item.inCart = value

					doAsync {
						cart.saveItems()
					}
				}

				// Launch the edit item activity when long clicked, and do not
				// register a regular click event
				component.layoutItem.setOnLongClickListener {
					val intent = Intent(this,
							            EditItemActivity::class.java).apply {
						putExtra("itemID", item.id)
					}
					startActivity(intent)

					true
				}

				// Add the item component to the list
				editListView.addView(component.layoutItem)
			}
		} else {
			editListView.addView(MessageScreenComponent(this,
					R.string.make_list_no_items,
					R.drawable.ic_shopping_cart_accent_24dp).layoutItem)
		}
	}

	/**
	 * (Asynchronous) Load the items from the database into the shopping cart,
	 * and populate the screen.
	 *
	 * Initialize the database using the passed context, load all of the items
	 * from the database, and then call the populateScreen method to draw all
	 * items onto the screen.
	 *
	 * @param context The application context, for database initialization.
	 */
	private fun loadData(context: Context) = doAsync {
		cart.initializeDB(context)
		cart.loadItems()

		uiThread {
			populateScreen()
		}
	}
}
