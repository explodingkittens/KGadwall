package xyz.benhigh.kgadwall

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_edit_item.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import xyz.benhigh.kgadwall.backend.Item
import xyz.benhigh.kgadwall.backend.ShoppingCart
import java.util.*

/**
 * Activity for creating. editing, and deleting items in the cart.
 *
 * This activity takes in an integer value via an intent bundle, with the key
 * "itemID". This value can be any number between 0 and Kotlin's maximum
 * integer value, inclusively. If the value is 0, then the activity assumes the
 * user is intending to create a new item. Otherwise, the activity assumes the
 * user is intending to modify an existing item, and will search the database
 * for the item. If the value is not 0, it should point to a GUID of an item in
 * the shopping cart.
 *
 * It should never occur under normal circumstances, but if the activity is
 * passed a nonzero item ID that does not correspond to an existing item in the
 * cart, then it will act as if the user is attempting to create a new item,
 * although the delete button will still appear. Saving the item under this
 * circumstance will simply create a new item, while deleting will do nothing.
 *
 * @author Ben High
 */
class EditItemActivity : AppCompatActivity() {
	private val cart = ShoppingCart()

	// Use a blank target item as a default item for the change detection
	private var targetItem = Item(0, "", "", false, 0)
	private var targetId = 0

	/**
	 * Create the activity, get the bundled ID, and load the database.
	 *
	 * This activity takes in an item's GUID value via a bundle extra. This
	 * method extracts that GUID, setting the targetId attribute to the GUID,
	 * and then calls the asynchronous method to load items into the cart.
	 *
	 * @param savedInstanceState The save state for the activity.
	 */
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_edit_item)

		// Check the bundle and get the target ID
		targetId = intent.extras.getInt("itemID")

		// Load in the database
		loadData(this)
	}

	/**
	 * Initializes the options menu.
	 *
	 * This method initializes the options menu for the action bar, found in
	 * /res/menu/edit_item_menu.xml. Additionally, this method will also check
	 * if an item's GUID has been passed in (which would indicate that the
	 * activity is editing an item). If it finds no GUID passed in, then the
	 * menu removes the delete option, as deleting an object that doesn't even
	 * exist makes approximately no sense at all.
	 *
	 * @param menu The menu in which to inflate our menu.
	 * @return Always true because we want to show the options menu.
	 */
	override fun onCreateOptionsMenu(menu: Menu?): Boolean {
		menuInflater.inflate(R.menu.edit_item_menu, menu)

		// Remove the delete button if we're working with a new item
		if (targetId == 0) {
			menu!!.removeItem(R.id.editItemDeleteMenuButton)
		}

		return true
	}

	/**
	 * Bind actions to the delete, save, and back menu buttons.
	 *
	 * The delete button makes a call to deleteAndFinish(). The save button
	 * makes a call to saveAndFinish(). The back button is an alias to
	 * Android's system UI back button.
	 *
	 * @param item The selected menu item.
	 * @return Always true because we want to consume the tap action.
	 */
	override fun onOptionsItemSelected(item: MenuItem?): Boolean {
		when {
			item!!.itemId == R.id.editItemDeleteMenuButton -> {
				deleteAndFinish()
			}
			item.itemId == R.id.editItemSaveMenuButton -> {
				saveAndFinish()
			}
			item.itemId == android.R.id.home -> {
				onBackPressed()
			}
		}

		return true
	}

	/**
	 * Checks if there are changes to the item, and displays a prompt.
	 *
	 * This method, also called when tapping the back button on the menu,
	 * checks if the entered name and aisle values differ from the respective
	 * values on the target item. If so, an alert is displayed, giving the user
	 * the option to save or discard their changes, or simply cancel the exit
	 * event. If no changes are detected, the activity simply finishes.
	 */
	override fun onBackPressed() {
		// Only prompt the user to save when the name or aisle inputs hold
		// different values from the target item
		if (itemNameInput.text.toString() != targetItem.name ||
				itemAisleInput.text.toString() != targetItem.aisle) {
			// Show a warning popup, giving the user the ability to save or
			// discard their changes
			val alert = AlertDialog.Builder(this)

			alert.setMessage(R.string.save_discard_message_title)
			alert.setPositiveButton(R.string.save_discard_message_save) { _, _ ->
				saveAndFinish()
			}
			alert.setNegativeButton(R.string.save_discard_message_discard) { _, _ ->
				finish()
			}
			alert.setNeutralButton(android.R.string.cancel) { dialog, _ ->
				dialog.dismiss()
			}
			alert.show()
		} else {
			finish()
		}
	}

	/**
	 * (Asynchronous) Removes an item and closes the activity.
	 *
	 * This method simply calls the removeItem method in the shopping cart, and
	 * finishes the activity on the UI thread.
	 */
	private fun deleteAndFinish() = doAsync {
		cart.removeItem(targetItem)

		// Finish the activity on the UI thread
		uiThread {
			finish()
		}
	}


	/**
	 * Confirm that the values in the EditText components are valid.
	 *
	 * This method more only confirms that the values are not empty or just
	 * whitespace.
	 *
	 * @return True if the inputs are valid, false if they are not.
	 */
	private fun validateInput(): Boolean {
		return (!itemNameInput.text.toString().isBlank()
				&& !itemAisleInput.text.toString().isBlank())
	}

	/**
	 * Save the item and finish this activity.
	 *
	 * This method checks with validateInput to confirm that the values in the
	 * EditText components are valid. If they are, the method launches an
	 * asynchronous task, which either adds the item to the cart, or modifies
	 * the existing item and saves the cart's items to the database, depending
	 * on whether an item GUID was passed in or not. Finally, the task finishes
	 * the activity from a UI thread. If the validation failed, an alert is
	 * shown to advise the user to check their inputs.
	 */
	private fun saveAndFinish() {
		if (validateInput()) {
			doAsync {
				// Create a new item if the target item is blank. Otherwise,
				// edit the target item. We want to check if the target ITEM's
				// ID is 0, as opposed to the target ID being 0, in case this
				// activity is sent a garbage ID
				if (targetItem.id == 0) {
					val newItem = Item(generateRandomUniqueID(),
							itemNameInput.text.toString(),
							itemAisleInput.text.toString(),
							false,
							System.currentTimeMillis() / 1000)

					cart.addItem(newItem)
				} else {
					targetItem.name = itemNameInput.text.toString().trim()
					targetItem.aisle = itemAisleInput.text.toString().trim()
					targetItem.lastEdit = System.currentTimeMillis() / 1000

					cart.saveItems()
				}

				// Finish the activity on the UI thread
				uiThread {
					finish()
				}
			}
		} else {
			// Show an error alert if the inputs did not pass validation
			val errorAlert = AlertDialog.Builder(this)

			errorAlert.setTitle(R.string.edit_item_empty_fields_title)
			errorAlert.setMessage(R.string.edit_item_empty_fields_error)
			errorAlert.setPositiveButton(android.R.string.ok) { dialog, _ ->
				dialog.dismiss()
			}
			errorAlert.show()
		}
	}

	/**
	 * (Asynchronous) Load the items from the database into the shopping cart
	 * object, and get the target item, if there is one.
	 *
	 * This method asynchronously initializes the database and then loads all
	 * items into the shopping cart. After that, it searches through the cart's
	 * items in order to find the target item, assuming the target ID is not 0.
	 * In the event that the item cannot be found, the default blank item is
	 * left as the target item. The saveAndFinish and deleteAndFinish methods
	 * know how to handle such events. Once the item is found, the UI thread is
	 * invoked to set the default values in both EditText components.
	 *
	 * @param context The application context, for database initialization.
	 */
	private fun loadData(context: Context) = doAsync {
		cart.initializeDB(context)
		cart.loadItems()

		// IDs can never be greater than 0, so if that's passed in, our target
		// item is blank, so do nothing. Otherwise, get the target item from
		// the cart. If the item cannot be identified, then the blank item will
		// be used
		if (targetId > 0) {
			for (item in cart.items) {
				if (item.id == targetId) {
					targetItem = item
					break
				}
			}
		}

		uiThread {
			// Auto-fill the name and aisle values. If the targetItem is blank,
			// then the normal hint values will just be displayed
			itemNameInput.setText(targetItem.name)
			itemAisleInput.setText(targetItem.aisle)
		}
	}

	/**
	 * Generate a random GUID value.
	 *
	 * This method creates a list of existing GUID values among items in the
	 * cart, and then generates a new value between 1 and Kotlin's maximum
	 * integer value. If the newly created value already exists in the cart,
	 * the method generates a new one. This process repeats until a unique
	 * ID number has been generated, which is then returned.
	 *
	 * @return A unique numerical identifier value.
	 */
	private fun generateRandomUniqueID(): Int {
		// Create a list of existing IDs
		val existingIDs = ArrayList<Int>()
		for (item in cart.items) {
			existingIDs.add(item.id)
		}

		// Generate a new ID until a unique value is found
		var newID: Int
		val random = Random(System.currentTimeMillis())
		do {
			newID = random.nextInt(Int.MAX_VALUE - 1) + 1
		} while (existingIDs.contains(newID))

		return newID
	}
}
