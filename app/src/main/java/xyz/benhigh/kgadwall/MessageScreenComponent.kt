package xyz.benhigh.kgadwall

import android.content.Context
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

/**
 * Component to display a message, as well as optionally fade the message in.
 *
 * This component shows a drawable image and a text view inside of a linear
 * layout. By default, both the image and text will fade in for 1.5 seconds
 * when added to the screen. This can be disabled by setting the fadeIn
 * argument to false. ID's pointing to string and drawable resources should be
 * passed in for the message and image. When used in an activity, the
 * layoutItem is what should be added to the screen.
 *
 * @author Ben High
 */
class MessageScreenComponent(context: Context, messageID: Int, drawableID: Int, fadeIn: Boolean = true) {
	val layoutItem = LinearLayout(context)
	private val imageView = ImageView(context)
	private val textView = TextView(context)

	init {
		// Set up layout item
		layoutItem.layoutParams = ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT
		)
		layoutItem.orientation = LinearLayout.VERTICAL

		// Set up image view
		imageView.setImageDrawable(context.getDrawable(drawableID))
		val imageParams = LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT
		)
		imageParams.weight = 5.0f
		imageView.layoutParams = imageParams


		// Set up text view
		textView.text = context.getString(messageID)
		textView.textSize = 36.0f
		textView.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
		val textParams =  LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT
		)
		textParams.weight = 2.0f
		textView.layoutParams = textParams

		// Add items and fade in layout
		layoutItem.addView(imageView)
		layoutItem.addView(textView)

		// Set up the fade in
		if (fadeIn) {
			val fadeAnimation = AlphaAnimation(0.0f, 1.0f)
			fadeAnimation.duration = 1500
			layoutItem.startAnimation(fadeAnimation)
		}
	}
}